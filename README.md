# Cliente Websoket

Permite conectarse al servidor socker y desplegar la temperatura y hora de distintas ubicaciones.

### Configuración

La configuración se realizar en el archivo ubicado en **assets/js/config.js**

* Agrega la URL del servidor socket en la constante *SOCKET_URL*
* Agregar el intervalo de reintentos de conexión hacia el servidor en la constante *RECONNECT_INTERVAL*

### Correr Cliente

Para levantar el cliente lo puedes pubicar los archivos en un servidor web.

También lo puedes hacer de manera local. Solo necesitas colocar la ruta completa del archivo index.html en un Navegador y que desde tu red puedas llegar al servidor socket.
var app = new Vue({

    el:'#app',
    data: {

        locations: [],
        intervals: [],
        webSocket: null,
        showLoader:true,
        reconnectInterval: RECONNECT_INTERVAL || 3000,
        timeInterval: TIME_INTERVAL || 1000,
        socket_url: SOCKET_URL || null,

    },
    methods:{
        initSocket: function() {
            let _self = this;

            this.webSocket = new ReconnectingWebSocket(this.socket_url);
            this.webSocket.timeoutInterval = this.reconnectInterval;

            this.webSocket.onopen = function (event) {
              _self.showLoader = true;
              console.debug(_self.getTime(0) + ' conexion iniciada');
            };

            this.webSocket.onmessage = function (event) {
                let weather = JSON.parse(event.data);
                _self.showLoader = false;
                console.debug(_self.getTime(0), weather);
                if(Array.isArray(weather) && weather.length > 0)
                {
                    _self.updateWeather(JSON.parse(event.data));
                }
            }

            this.webSocket.onclose = function(e){
                console.debug(_self.getTime(0) + ' Cierra conexion anterior');
                _self.showLoader = _self.locations && _self.locations.length == 0 ;
            };
        },
        updateWeather: function(_locations) {
            let _self = this;

            console.log(this.getTime(0) + ' Actualiza lista de paises');

            this.intervals.forEach( function(interval, index) {
                clearInterval(interval);
                _self.intervals.slice(index,1);
            });

            this.locations = _locations.map( location => {
                location.time = _self.getTime(location.offset);
                let interval = setInterval(function() {
                    location.time = _self.getTime(location.offset);
                }, this.timeInterval);
                this.intervals.push(interval);
                return location;
            });

        },
        getTime: function(hours){
            return moment().add(hours,'hours').format('HH:mm:ss');
        }
    },
    beforeMount(){
        this.initSocket();
    },
});
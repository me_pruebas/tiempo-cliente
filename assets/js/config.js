/*
* Intervalo para realizar una reconexión con el servidor.
* El tiempo está en milisegundos.
*/
const RECONNECT_INTERVAL = 60000;
/*
* intervalo con el que se actuliza la hora
* El tiempo está en milisegundos.
*/
const TIME_INTERVAL =  1000;
/*
* URL del servidor y puerto del servidor
*/
const SOCKET_URL = 'ws://localhost:8080';